--- install and execute create-vue, the official Vue project scaffolding tool
ref. https://vuejs.org/guide/quick-start.html#creating-a-vue-application

npm init vue@latest

--- install & run 
yarn install
yarn dev

--- rm auto-generated code
(pls view in git log)

--- follow tutorial
ref. https://vuejs.org/tutorial/#step-2

== nn note
v-bind:propname to bind data()'s :field                    to html property in :template 
v-bind:propname to bind data()'s :formfield                to html property in :template 

v-on:eventname  to bind event eg onclick, onchange, etc.   to method in :methods in :script

v-model
//             text fieldname defined in data()
//                           onInput to copy this.text = e.target.value - defeind in :methods
<input :value="text" @input="onInput">
<input v-model="text">
