import { createApp } from 'vue'
import './assets/main.css'

// import App from './tutorial_App/App02_template_var.vue'      ; createApp(App).mount('#app')
// import App from './tutorial_App/App03_:prop_v-bind:prop.vue' ; createApp(App).mount('#app')
// import App from './tutorial_App/App04_v-on:event_@event.vue' ; createApp(App).mount('#app')
// import App from './tutorial_App/App05_v-model:xx.vue'        ; createApp(App).mount('#app')
// import App from './tutorial_App/App06_ifelse.vue'            ; createApp(App).mount('#app')
import App from './tutorial_App/App07_loop.vue'                 ; createApp(App).mount('#app')
